#!/usr/bin/env python

import argparse
import mmap
import sys
from ast import literal_eval
from glob import glob
from importlib import import_module
from os.path import basename, join, dirname, realpath, abspath

import numpy as np
import torch
import torch.utils.data as t_data

sys.path.insert(0, realpath(join(dirname(__file__), '..')))

from base.config_loader import ConfigLoader
from base.data.dataloader import DresdenDataloader
from base.logger import Logger
from base.evaluation.evaluate_model import evaluate_classifier
from base.evaluation.result_logger import ResultLogger


def parse():
    parser = argparse.ArgumentParser(description='evaluate exp')
    parser.add_argument('--experiment', '-e', required=True,
                        help='name of experiment to run')
    parser.add_argument('--dataset', '-d', default='valid',
                        help='dataset to evaluate model on')
    parser.add_argument('--model', '-m', default='best',
                        help='model to evaluate')

    return parser.parse_args()


def get_model_data_fraction_map():
    """search through all logfiles beginning with 'clas_' for all trained models and data fractions used to train these
    models

    Returns:
        dict: map of 'model file_name': 'data fraction' pairs
    """
    model_data_fraction_map = {}
    for log_file in glob(join(config.EXPERIMENT_DIR, 'clas_*.log')):
        with open(log_file, 'rb', 0) as file, mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
            models = []

            # search files for DATA_FRACTIONS configuration at the start and parse them to a list
            idx = s.find(b'DATA_FRACTIONS') - s.tell()
            s.read(idx)
            data_fractions_line = str(s.readline(), 'utf-8')
            data_fractions = literal_eval(
                data_fractions_line[data_fractions_line.find('['):data_fractions_line.find(']') + 1])

            # search for model file names saved as additional snapshots (best models are written over)
            search_string = 'additional snapshot saved to'
            while s.find(bytes(search_string, 'utf-8')) != -1:
                idx = s.find(bytes(search_string, 'utf-8')) - s.tell()
                s.read(idx)
                model_line = str(s.readline(), 'utf-8')
                models.append(model_line[model_line.rfind('/') + 1:model_line.find('.pth') + 4])

            # map model file names to data fractions
            unique_models = np.unique(models)
            unique_models.sort()
            for m, df in zip(unique_models, data_fractions):
                model_data_fraction_map[m] = df
    return model_data_fraction_map


def load_models_for_eval():
    """load all models to evaluate

    Returns:
        (list, list, dict): lists of cpc models and classifiers and a dict mapping the model file names to daa fractions
    """
    # save models and model file names
    cpc_models, clas_models = [], []
    cpc_file_names, clas_file_names = [], []
    # python modules to create cpc models and classifiers from
    cpc_model_module = import_module('.' + config.CPC_MODEL_NAME, 'base.models')
    clas_model_module = import_module('.' + config.CLAS_MODEL_NAME, 'base.models')

    # based on the configuration use the cpc model created in the same experiment or from a different experiment
    if config.CLAS_CPC_MODEL_FILE == 'same':
        cpc_model_path = join(config.EXPERIMENT_DIR, config.CPC_MODEL_NAME + '-best.pth')
    else:
        cpc_model_path = join(config.EXPERIMENT_DIR, '../..', config.CLAS_CPC_MODEL_FILE[:6],
                              config.CLAS_CPC_MODEL_FILE)

    # if only the best model is to be evaluated, use the one in the experiment_dir
    if args.model == 'best':
        cpc_file_names.append(cpc_model_path)
        clas_file_names.append(join(config.EXPERIMENT_DIR, config.CLAS_MODEL_NAME + '-best.pth'))
    # if all models are to be evaluated, use the models saved as additional snapshots in experiment_dir/models
    elif args.model == 'all':
        # load all run_names first, a set is used, so cpc models and classifiers with the same run_name do not get
        # evaluated multiple times
        run_names = set()
        for file in glob(join(join(config.EXPERIMENT_DIR, 'models'), 'clas_*.pth')):
            run_names.add(file[file.index('-') + 1:].replace('.pth', ''))
        # load model file names for every run_name and add them to the models to evaluate
        for rn in sorted(run_names):
            if config.CLAS_CPC_LR is not None:
                cpc_model_path = join(config.EXPERIMENT_DIR, 'models',
                                      config.CPC_MODEL_NAME + '-' + rn + '.pth')
            clas_model_path = join(config.EXPERIMENT_DIR, 'models', config.CLAS_MODEL_NAME + '-' + rn + '.pth')
            cpc_file_names.append(cpc_model_path)
            clas_file_names.append(clas_model_path)

    # load model file_name to data fractions map
    model_data_fraction_map = get_model_data_fraction_map()
    data_fractions = []

    # iterate over all file names and create the corresponding models
    for i, (cpc, clas) in enumerate(zip(cpc_file_names, clas_file_names), start=1):
        logger.info('model number {}'.format(i))

        # create cpc model
        cpc_model = cpc_model_module.Model(config).to(config.DEVICE).eval()
        cpc_model.load_state_dict(torch.load(cpc)['state_dict'])
        logger.info('cpc model loaded from ' + abspath(cpc))
        cpc_models.append(cpc_model)

        # create classifier
        clas_model = clas_model_module.Model(config, cpc_model.get_feature_size()).to(config.DEVICE).eval()
        clas_model.load_state_dict(torch.load(clas)['state_dict'])
        logger.info('classifier loaded from ' + abspath(clas))
        clas_models.append(clas_model)

        # log the data fraction used for training the classifier
        clas_model_file = clas[clas.rfind('/') + 1:]
        if clas_model_file in model_data_fraction_map:
            logger.info(f'data fraction: {model_data_fraction_map[clas_model_file]:g}')
            data_fractions.append(model_data_fraction_map[clas_model_file])
        else:
            logger.info(f'model {clas_model_file} can not be found in any log file, '
                        f'maybe this is an old run that can be deleted?')
            data_fractions.append(0)
        logger.info('')

    return cpc_models, clas_models, data_fractions


def evaluate_model(cpc_model, clas_model):
    """evaluates clas_model with features from cpc_model

    Returns:
        float: avg f1 score on given datatset
    """
    logger.fancy_log('start evaluation')
    result_logger = ResultLogger(config)

    # create dataloader for given dataset, the data should not be altered in any way
    map_loader = DresdenDataloader(config, args.dataset, balanced=False, augment_data=False, classifier=True)
    dataloader = t_data.DataLoader(map_loader, batch_size=config.BATCH_SIZE_EVAL, shuffle=False, num_workers=4)

    logger.info('cpc model:\n' + str(cpc_model))
    logger.info('classifier:\n' + str(clas_model))

    # evaluate model
    labels, _ = evaluate_classifier(config, cpc_model, clas_model, dataloader)

    # log/plot results
    f1_scores = result_logger.log_sleep_stage_f1_scores(labels['actual'], labels['predicted'], args.dataset)
    logger.info('')
    result_logger.log_confusion_matrix(labels['actual'], labels['predicted'], args.dataset, wo_plot=False)
    result_logger.log_transformation_matrix(labels['actual'], labels['predicted'], args.dataset, wo_plot=False)

    logger.fancy_log('finished evaluation')
    return f1_scores['avg']


def evaluate_all_models():
    # load models to be evaluateda dn a map mapping a data fraction to each model
    cpc_models, clas_models, data_fractions = load_models_for_eval()
    # evaluate all models and save their f1 scores
    avg_f1_scores = []
    for i, (cpc, clas) in enumerate(zip(cpc_models, clas_models), start=1):
        logger.fancy_log(f'start evaluation of model number {i}')
        avg_f1_score = evaluate_model(cpc, clas)
        avg_f1_scores.append(avg_f1_score)

    # map f1 scores to data fractions and log the resulting dict
    df_f1_scores_map = {}
    for df, f1 in zip(data_fractions, avg_f1_scores):
        if df in df_f1_scores_map:
            df_f1_scores_map[df].append(f1)
        else:
            df_f1_scores_map[df] = [f1]
    logger.info(str(df_f1_scores_map))


if __name__ == '__main__':
    args = parse()
    config = ConfigLoader(args.experiment)

    logger = Logger(config)  # wrapper for logger
    logger.init_log_file(args, basename(__file__))  # create log file and log config, etc

    logger.fancy_log(f'evaluate {args.model} model(s) of experiment {args.experiment} on dataset {args.dataset}')
    evaluate_all_models()
