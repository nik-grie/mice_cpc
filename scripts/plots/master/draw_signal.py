import sys
from os.path import join, dirname, realpath

import matplotlib.pyplot as plt
import numpy as np

sys.path.insert(0, realpath(join(dirname(__file__), '../../..')))

from base.config_loader import ConfigLoader
from base.data.dataloader import DresdenDataloader

view_config = ConfigLoader(create_dirs=False)
view_config.DATA_FILE = '../../../cache/dataset/data_dresden.h5'
# only load the middle epoch, so the epochs can be concatenated more easily
view_config.EPOCHS_LEFT, view_config.EPOCHS_RIGHT = 0, 0
datamap_view = DresdenDataloader(view_config, 'valid', False, False)
# epoch to start loading data from
cur_epoch = 60986

plt.figure(figsize=(12, 2))
plt.axis('off')
colors = {'EEG1': 'darkgreen', 'EEG2': 'limegreen', 'EMG1': 'darkred'}
# load 4 epochs starting from cur_sample
signal = np.concatenate([datamap_view[cur_epoch + i][0][2] for i in range(1, 5)])
# plot desired signal, in this case the EMG signal
plt.plot(signal, c=colors['EMG1'])
minmax = 2.5
plt.ylim((-minmax, minmax))
plt.tight_layout()
plt.show()
