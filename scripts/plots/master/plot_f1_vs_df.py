import locale
import sys
from os.path import realpath, join, dirname

import matplotlib.pyplot as plt
import numpy as np

from scripts.plots.master.scores.validation_f1_vs_df import *
from scripts.plots.master.scores.test_f1_vs_df import *

sys.path.insert(0, realpath(join(dirname(__file__), '../../..')))

if __name__ == '__main__':
    # 'name' is the name of the file the plot is saved in
    # 'exps' is a dict containing the scores of the experiments to plot and the label of the plot in the legend

    # name = 'best'
    # exps = {'fully supervised': val_supervised, 'cpc fixiert': val_002c, 'cpc fine-tuned': val_002f}
    name = 'best test'
    exps = {'fully supervised': test_supervised, 'CPC fixiert': test_002c, 'CPC fine-tuned': test_002f}

    # type of plot to create, median of all repetitions or boxplot over repetitions
    mode = 'median'
    # mode = 'boxplot'

    # maximum value on x-axis
    limit = 0.01

    colors = ['blue', 'green', 'orange', 'red', 'cyan',
              'brown', 'pink', 'gray', 'olive', 'cyan']

    # formatting options
    locale.setlocale(locale.LC_NUMERIC, "de_DE")
    plt.figure(figsize=(10, 5))
    plt.rcParams.update({'font.size': 16})
    plt.rcParams['axes.formatter.use_locale'] = True

    # save boxplot objects for labeling in the legend
    bps = {}
    for i, exp in enumerate(exps):
        x = [k * 1 for k in exps[exp].keys() if k <= limit]
        if mode == 'median':
            print([f'{np.mean(v):.2f}' for k, v in exps[exp].items() if k <= limit])
            plt.plot(x, [np.mean(v) for k, v in exps[exp].items() if k <= limit], marker='o', label=exp, c=colors[i])
        elif mode == 'boxplot':
            bps[exp] = plt.boxplot([exps[exp][df] for df in exps[exp] if df <= limit], positions=x,
                                   widths=[xi / 8 for xi in x], patch_artist=True,
                                   boxprops=dict(facecolor=colors[i]), sym='+')
            for box in bps[exp]['boxes']:
                r, g, b, a = box.get_facecolor()
                box.set_facecolor((r, g, b, 0.5))
            plt.setp(bps[exp]['caps'], linewidth=1)
            plt.setp(bps[exp]['medians'], color=colors[i])
            plt.setp(bps[exp]['whiskers'], linewidth=1, linestyle='--')
            plt.setp(bps[exp]['fliers'], markersize=7, markeredgecolor=colors[i])

    max_f1_score = max([max(exps[exp][1]) for exp in exps if len(exps[exp][1]) > 0])
    max_line = plt.hlines(max_f1_score, 0, 2, colors='red', linestyles='--', label='bester F1 Score\nbei 100% Tr.daten')

    if mode == 'boxplot':
        leg = plt.legend([bp['boxes'][0] for bp in bps.values()] + [max_line],
                         [e for e in bps] + ['bester F1 Score\nbei 100% Tr.daten'], loc='lower right')
        for lh in leg.legendHandles:
            lh.set_alpha(1)
    else:
        plt.legend(loc='lower right')
    plt.grid()
    plt.semilogx()
    # x_axis_ticks = list(np.unique([k for exp in exps for k in exps[exp].keys()]))
    x_axis_ticks = [10 ** -i for i in range(5, -1, -1)]
    plt.xticks(x_axis_ticks, ['{:g}%'.format(k * 100).replace('.', ',') for k in x_axis_ticks])
    plt.xlim((8 / 10 * x_axis_ticks[0], 10 / 8 * limit))
    plt.ylim((0.5, 1))
    plt.xlabel('Anteil der Trainingsdaten')
    plt.ylabel('gemittelter F1 Score')
    plt.tight_layout()
    plt.savefig('../../../results/plots/master/' + name.replace(' ', '_') + '-' + mode + '.svg')
    plt.show()
