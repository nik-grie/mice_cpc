import locale

import matplotlib.pyplot as plt
import numpy as np

baseline_009 = [0.01667715, 0.01563821, 0.01515612, 0.01583414, 0.01579547, 0.01575938,
                0.01490864, 0.01590117, 0.01581867, 0.01556345, 0.01557118, 0.01561501, ]
train_accs_009 = [0.99840749, 0.99329774, 0.93789639, 0.7749828, 0.7644135, 0.75698808,
                  0.75057946, 0.74576763, 0.74101025, 0.73747543, 0.73344807, 0.66230595, ]
valid_accs_009 = [0.85502815, 0.71638532, 0.24425877, 0.05179223, 0.04732712, 0.04388805,
                  0.04218657, 0.04026079, 0.03878359, 0.03759255, 0.03632417, 0.03430559, ]

# format text for german numbers and a bigger font
locale.setlocale(locale.LC_NUMERIC, "de_DE")
plt.figure(figsize=(8, 6))
plt.rcParams.update({'font.size': 12})
plt.rcParams['axes.formatter.use_locale'] = True

t = np.arange(1, len(train_accs_009) + 1)
plt.plot(t, train_accs_009, label='bestes Modell auf Train.dat.')
plt.plot(t, valid_accs_009, label='bestes Modell auf Val.dat.')
plt.plot(t, baseline_009, label='untrainiert auf Val.dat.', c='red', ls='--')
plt.legend()
plt.grid()

plt.xlabel('Vorhersageschritt')
plt.ylabel('Accuracy')
plt.tight_layout()

plt.savefig('../../../results/plots/master/accs_vs_timesteps.svg')
plt.show()
