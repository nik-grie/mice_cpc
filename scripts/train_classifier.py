#! /usr/bin/env python

import argparse
import sys
import time
from importlib import import_module
from os.path import basename, realpath, join, dirname, abspath

import torch
import torch.utils.data as t_data

sys.path.insert(0, realpath(join(dirname(__file__), '..')))

from base.config_loader import ConfigLoader
from base.data.dataloader import DresdenDataloader
from base.evaluation.evaluate_model import evaluate_classifier
from base.evaluation.result_logger import ResultLogger
from base.logger import Logger
from base.training.scheduled_optim import ScheduledOptim
from base.training.train_model import train_classifier, snapshot


def parse():
    """define and parse arguments for the script"""
    parser = argparse.ArgumentParser(description='training script')
    parser.add_argument('--experiment', '-e', required=True,
                        help='name of experiment to run')

    return parser.parse_args()


def load_cpc_model():
    # create model from cpc_model_name given in config and load it onto configured DEVICE
    cpc_model = import_module('.' + config.CPC_MODEL_NAME, 'base.models').Model(config).to(config.DEVICE)
    logger.info('feature extractor:\n' + str(cpc_model))

    # if CLAS_CPC_MODEL_FILE is 'none' the randomly initialized cpc model is used
    if config.CLAS_CPC_MODEL_FILE != 'none':
        # if CLAS_CPC_MODEL_FILE is 'same' the cpc model from the current experiment is used
        if config.CLAS_CPC_MODEL_FILE == 'same':
            path = join(config.EXPERIMENT_DIR, config.CPC_MODEL_NAME + '-best.pth')
        # else the cpc model from the configured experiment is used
        else:
            path = join(config.EXPERIMENT_DIR, '../..', config.CLAS_CPC_MODEL_FILE[:6], config.CLAS_CPC_MODEL_FILE)
        model_args = torch.load(path)
        cpc_model.load_state_dict(model_args['state_dict'])
        logger.info('feature extractor loaded from ' + abspath(path))

    # create an additional optimizer for the cpc model if fine-tuning is configured (by setting CLAS_CPC_LR)
    cpc_optimizer = None
    if config.CLAS_CPC_LR is None:
        for param in cpc_model.parameters():
            param.requires_grad = False
    else:
        cpc_optimizer = ScheduledOptim(
            config.CLAS_OPTIMIZER(filter(lambda p: p.requires_grad, cpc_model.parameters()),
                                  weight_decay=config.CLAS_L2_DECAY, **config.CLAS_OPTIM_PARAS),
            peak_lr=config.CLAS_CPC_LR, warmup_epochs=config.CLAS_WARMUP_EPOCHS, total_epochs=config.CLAS_EPOCHS,
            parameters=config.CLAS_S_OPTIM_PARAS, mode=config.CLAS_S_OPTIM_MODE)

    return cpc_model, cpc_optimizer


def load_classifier(feature_size):
    # create model from clas_model_name given in config and load it onto configured DEVICE
    clas_model = import_module('.' + config.CLAS_MODEL_NAME, 'base.models').Model(config, feature_size).to(
        config.DEVICE)
    logger.info('classifier:\n' + str(clas_model))
    # scheduled optimizer containing the pytorch optimizer with parameters from config
    clas_optimizer = ScheduledOptim(
        config.CLAS_OPTIMIZER(filter(lambda p: p.requires_grad, clas_model.parameters()),
                              weight_decay=config.CLAS_L2_DECAY, **config.CLAS_OPTIM_PARAS),
        peak_lr=config.CLAS_LR, warmup_epochs=config.CLAS_WARMUP_EPOCHS, total_epochs=config.CLAS_EPOCHS,
        parameters=config.CLAS_S_OPTIM_PARAS, mode=config.CLAS_S_OPTIM_MODE)

    return clas_model, clas_optimizer


def evaluate_epoch_zero(cpc_model, clas_model, trainloader, validationloader, f1_scores):
    labels_train, _ = evaluate_classifier(config, cpc_model, clas_model, trainloader)
    f1_scores_tr_0 = ResultLogger(config).log_sleep_stage_f1_scores(labels_train['actual'], labels_train['predicted'],
                                                                    'train')
    for stage in f1_scores_tr_0:
        f1_scores['train'][stage].append(f1_scores_tr_0[stage])

    labels_valid, _ = evaluate_classifier(config, cpc_model, clas_model, validationloader)
    f1_scores_va_0 = ResultLogger(config).log_sleep_stage_f1_scores(labels_valid['actual'], labels_valid['predicted'],
                                                                    'valid')
    for stage in f1_scores_va_0:
        f1_scores['valid'][stage].append(f1_scores_va_0[stage])


def train_classifier_on_data_fraction(data_fraction):
    """train classifier as it is described in config"""
    result_logger = ResultLogger(config)  # wrapper for various methods to log/plot results

    # train dataloader with configured data augmentation and rebalancing on given data_fraction
    dl_train = DresdenDataloader(config, 'train', balanced=config.BALANCED_TRAINING, augment_data=True, classifier=True,
                                 data_fraction=data_fraction)
    # validation dataloader without modification of loaded data
    dl_valid = DresdenDataloader(config, 'valid', balanced=False, augment_data=False, classifier=True,
                                 data_fraction=1.0)

    # calculate reduced batch size based on the number of samples in train data
    # batch size must at least be 2 for BN layers to work and should lead to at least 5 full mini batches
    reduced_batch_size = max(min(config.CLAS_BATCH_SIZE, int(len(dl_train) / 5)), 2)
    logger.info(f'run exp on {data_fraction * 100:g}% of train data with batch size: {reduced_batch_size}')

    # replicate samples in train dataloader to get the same number of gradient updates per epoch as 100% of train data
    # would get
    # number of mini batches with 100% of train data
    total_num_mini_batches = len(dl_train) / data_fraction / config.CLAS_BATCH_SIZE
    # factor to replicate train samples by
    repl_factor = max(int(total_num_mini_batches / len(dl_train) * reduced_batch_size), 1)
    logger.info(f'repeat samples {repl_factor} times to keep number of gradient updates per epoch consistent '
                f'for all data fractions')
    # replicate samples
    dl_train.repeat_indices(repl_factor)

    # multithreaded pytorch dataloaders with 4 workers each, train data is shuffled
    trainloader = t_data.DataLoader(dl_train, batch_size=reduced_batch_size, shuffle=True, num_workers=4)
    validationloader = t_data.DataLoader(dl_valid, batch_size=config.BATCH_SIZE_EVAL, shuffle=False, num_workers=4)

    cpc_model, cpc_optimizer = load_cpc_model()
    clas_model, clas_optimizer = load_classifier(cpc_model.get_feature_size())

    # save best results for early stopping and snapshot creation of best model
    best_epoch = 0
    best_avg_f1_score = 0
    # save metrics after each epoch for plots
    f1_scores = {'train': {stage: [] for stage in config.STAGES + ['avg']},
                 'valid': {stage: [] for stage in config.STAGES + ['avg']}}
    losses = {'train': [], 'valid': []}

    evaluate_epoch_zero(cpc_model, clas_model, trainloader, validationloader, f1_scores)

    # iterate over EPOCHS, start with epoch 1
    for epoch in range(1, config.CLAS_EPOCHS + 1):
        start = time.time()  # measure time each epoch takes
        if epoch > 1:
            dl_train.reset_indices()  # rebalance samples for each epoch

        # train epoch and save metrics
        labels_train, loss_train = train_classifier(config, epoch, cpc_model, clas_model, cpc_optimizer, clas_optimizer,
                                                    trainloader)
        losses['train'].append(loss_train)

        # evaluate epoch and save metrics
        labels_valid, loss_valid = evaluate_classifier(config, cpc_model, clas_model, validationloader)
        losses['valid'].append(loss_valid)

        # calculate f1-scores for given training and validation labels and log them
        logger.info('')
        f1_scores_train = result_logger.log_sleep_stage_f1_scores(labels_train['actual'], labels_train['predicted'],
                                                                  'train')
        for stage in f1_scores_train:
            f1_scores['train'][stage].append(f1_scores_train[stage])
        f1_scores_valid = result_logger.log_sleep_stage_f1_scores(labels_valid['actual'], labels_valid['predicted'],
                                                                  'valid')
        for stage in f1_scores_valid:
            f1_scores['valid'][stage].append(f1_scores_valid[stage])

        # model from the current epoch better than best model?
        new_best_model = f1_scores_valid['avg'] > best_avg_f1_score
        # log/plot confusion and transformation matrices
        result_logger.log_confusion_matrix(labels_train['actual'], labels_train['predicted'], 'train', wo_plot=True)
        result_logger.log_confusion_matrix(labels_valid['actual'], labels_valid['predicted'], 'valid',
                                           wo_plot=not new_best_model)
        result_logger.log_transformation_matrix(labels_valid['actual'], labels_valid['predicted'], 'valid',
                                                wo_plot=not new_best_model)

        # save model if it updates the best model
        if new_best_model:
            best_avg_f1_score = f1_scores_valid['avg']
            best_epoch = epoch
            snapshot(config, {
                'model': config.CLAS_MODEL_NAME,
                'epoch': epoch,
                'validation_avg_f1_score': f1_scores_valid['avg'],
                'state_dict': clas_model.state_dict(),
                'clas_optimizer': clas_optimizer.state_dict(),
            }, config.CLAS_MODEL_NAME, save_models or config.EXTRA_SAFE_MODELS)
            # save fine-tuned cpc model
            if config.CLAS_CPC_LR is not None:
                snapshot(config, {
                    'model': config.CPC_MODEL_NAME,
                    'epoch': epoch,
                    'validation_avg_f1_score': f1_scores_valid['avg'],
                    'state_dict': cpc_model.state_dict(),
                    'clas_optimizer': cpc_optimizer.state_dict(),
                }, config.CPC_MODEL_NAME, save_models or config.EXTRA_SAFE_MODELS)

        end = time.time()
        logger.info('[epoch {:3d}] execution time: {:.2f}s\t avg f1-score: {:.4f}\n'.format(
            epoch, (end - start), f1_scores_valid['avg']))

        # increase epoch in scheduled optimizer to update the learning rate
        clas_optimizer.inc_epoch()
        if config.CLAS_CPC_LR is not None:
            cpc_optimizer.inc_epoch()

        # early stopping
        # stop training if the validation f1 score has not increased over the last 5 epochs
        # but only do so after WARMUP_EPOCHS was reached
        if epoch >= config.CLAS_WARMUP_EPOCHS * 1.5 and epoch - best_epoch > 4:
            break

    # log/plot f1-score course and metrics over all epochs for both datasets
    result_logger.plot_f1_score_course(f1_scores)
    result_logger.log_metrics({'loss': losses})
    logger.fancy_log('finished training')
    logger.fancy_log('best model on epoch: {} \tf1-score: {:.4f}'.format(best_epoch, best_avg_f1_score))
    return best_avg_f1_score


if __name__ == '__main__':
    args = parse()
    config = ConfigLoader(args.experiment, run_name_prefix='clas')  # load config from experiment

    logger = Logger(config)  # create wrapper for logger
    # create log_file and initialize it with the script arguments and the config
    logger.init_log_file(args, basename(__file__))

    logger.fancy_log('start training of classifier: {}'.format(config.CLAS_MODEL_NAME))

    # if more than one data fraction is configured, additional snapshots are forced
    save_models = len(config.DATA_FRACTIONS) > 1
    best_avg_f1_scores = {}

    for df in config.DATA_FRACTIONS:
        config = ConfigLoader(args.experiment, run_name_prefix='clas')
        best_avg_f1_scores[df] = train_classifier_on_data_fraction(df)

    if save_models:
        ResultLogger(config).plot_avg_f1_scores_vs_data_fractions(best_avg_f1_scores)
