import argparse
import sys
import time
from glob import glob
from os.path import join, basename, isfile, realpath, dirname, splitext

import numpy as np
import pandas as pd
import tables
from edfdb import EDF

sys.path.insert(0, realpath(join(dirname(__file__), '..')))

from base.data.downsample import downsample
from base.config_loader import ConfigLoader
from base.data.data_table import create_table_description, COLUMN_LABEL, COLUMN_MOUSE_ID

channel_synonyms = {
    'EEG1.1': 'EEG1', 'EEG1.2': 'EEG1', 'EEG1.3': 'EEG1', 'EEG1.4': 'EEG1',
    'EEG2.1': 'EEG2', 'EEG2.2': 'EEG2', 'EEG2.3': 'EEG2', 'EEG2.4': 'EEG2',
    'EMG.1': 'EMG1', 'EMG.2': 'EMG1', 'EMG.3': 'EMG1', 'EMG.4': 'EMG1', 'EMG': 'EMG1'
}


def parse():
    parser = argparse.ArgumentParser(description='data transformation script')
    parser.add_argument('--experiment', '-e', default='standard_config',
                        help='name of experiment to transform data to')

    return parser.parse_args()


def read_rid(rid: str):
    """reads data from files corresponding to `rid`

    Returns:
        (dict, list, list): features in the form of a dict with entries for each CHANNEL, labels as a list, list of
        start times of samples as indexes in features
    """
    edf = EDF.read_file(join(edfs_dir, rid + '.edf'))
    # set synonyms
    for channel in list(edf.channel_by_label.keys()):
        if channel in channel_synonyms:
            edf.channel_by_label[channel_synonyms[channel]] = edf.channel_by_label.pop(channel)
            edf.sampling_rate_by_label[channel_synonyms[channel]] = edf.sampling_rate_by_label.pop(channel)
    if not np.all([np.isin(c, list(edf.channel_by_label.keys())) for c in config.CHANNELS]):
        raise ValueError('skipped, there seems to be at least one missing channel: ' + str(edf.channel_by_label.keys()))

    data = edf.get_physical_samples(0, None, channels=config.CHANNELS)

    sampling_rates = np.array([int(edf.sampling_rate_by_label[c]) for c in config.CHANNELS])
    if np.all(sampling_rates == sampling_rates[0]):
        sr = sampling_rates[0]
    else:
        raise ValueError('sampling rates in original edf file are not equal:', sampling_rates)

    features = {}  # save features per CHANNEL
    # iterate over CHANNELS, load and downsample data
    for channel in config.CHANNELS:
        # downsample data to the SAMPLING_RATE specified in config
        features[channel] = downsample(data[channel], sr_old=sr, sr_new=config.SAMPLING_RATE,
                                       fmax=0.4 * config.SAMPLING_RATE, outtype='sos', method='pad')

    # read csv with labels and start times
    df = pd.read_csv(join(labels_dir, rid + '.csv'), parse_dates=[0])
    # rename columns to be consistent
    start_map = {'#Onset': 'start', 'Onset': 'start', '# start': 'start'}
    dt_map = {'Duration': 'dt', ' duration': 'dt'}
    annot_map = {'Annotation': 'label', ' annotation': 'label', 'annotation': 'label'}
    df.rename(columns=dict(**start_map, **dt_map, **annot_map), inplace=True)

    # load start times of samples and transform them to indexes in the feature map
    sample_start_times = (df['start'] - edf.startdatetime).dt.total_seconds().values
    sample_start_times = (sample_start_times * config.SAMPLING_RATE).astype('int')

    # load labels from data frame
    if config.STAGE_MAP['Artifact'] is None:
        labels = df['label'].apply(lambda x: x.strip(' X')).tolist()
    else:
        labels = df['label'].apply(lambda x: 'Artifact' if 'X' in x else x.strip()).tolist()

    return features, labels, sample_start_times


def write_data_to_table(table: tables.Table, features: dict, labels: list, start_times: list, mouse_id: str):
    """writes given data to the passed table, each sample is written in a new row"""
    sample = table.row

    # iterate over samples and create rows
    for sample_start, label in zip(start_times, labels):
        # determine idxs of data to load from features
        sample_end = int(sample_start + config.EPOCH_DURATION * config.SAMPLING_RATE)

        # try to load data from sample_start to sample_end, if there is not enough data, ignore the sample
        try:
            sample[COLUMN_MOUSE_ID] = mouse_id
            for c in config.CHANNELS:
                sample[c] = features[c][sample_start:sample_end]
            # map stage from h5 file to stage used for classification
            sample[COLUMN_LABEL] = config.STAGE_MAP[label]
            sample.append()
        except ValueError:
            print(f'not enough datapoints in sample, data from {sample_start} to {sample_end}')
            print(f'total number of data points in file: {len(list(features.values())[0])}')
            print('this sample is ignored')
            print('')
    # write data to table
    table.flush()


def get_files_for_dataset(dataset: str):
    """determines which files to transform for a given dataset, see DATA_SPLIT"""
    # load all rids in DATA_DIR and sort them
    rids = [splitext(basename(fn))[0] for fn in glob(join(labels_dir, '*'))]
    rids.sort()

    # search for files corresponding to rids in DATA_SPLIT
    rids_dataset = [r for mouse_name in config.DATA_SPLIT[dataset] for r in rids if mouse_name in r]

    return rids_dataset


def transform():
    """transform files in DATA_DIR to pytables table"""
    # load description of table columns
    table_desc = create_table_description(config)

    # if the transformed data file already exists, ask the user if he wants to overwrite it
    if isfile(config.DATA_FILE):
        if input(f'{realpath(config.DATA_FILE):s} already exists, do you want to override? (y/n)').lower() != 'y':
            exit()

    # open pytables DATA_FILE
    with tables.open_file(config.DATA_FILE, mode='w', title='data from Dresden') as f:

        # datasets to create, if there is a dataset without any data, an empty table is created inside DATA_FILE
        datasets = ['train', 'valid', 'test']

        for dataset in datasets:
            print('writing', dataset, 'data...')

            # create tables for every dataset
            table = f.create_table(f.root, dataset, table_desc, dataset + ' data')

            # determine which files to transform for each dataset based on DATA_SPLIT
            rids = get_files_for_dataset(dataset)
            # iterate over files, load them and write them to the created table
            for i, rid in enumerate(rids):
                print('mouse [{:d}/{:d}]: {:s}'.format(i + 1, len(rids), rid))
                start = time.time()

                # read files for rid, load and downsample data
                features, labels, times = read_rid(rid)
                # write loaded data to table
                write_data_to_table(table, features, labels, times, rid)

                print('execution time: {:.2f}'.format(time.time() - start))
                print()

        print(f)


if __name__ == '__main__':
    args = parse()
    # load config, dirs are not needed here, because we do not write a log
    config = ConfigLoader(args.experiment, create_dirs=False)

    labels_dir = join(config.DATA_DIR, 'ml-sleep-stages')
    edfs_dir = join(config.DATA_DIR, 'edfs')

    # transform files
    transform()
