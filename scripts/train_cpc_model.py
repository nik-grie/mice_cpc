#! /usr/bin/env python

import argparse
import sys
import time
from importlib import import_module
from os.path import basename, realpath, join, dirname

import numpy as np
import torch.utils.data as t_data

sys.path.insert(0, realpath(join(dirname(__file__), '..')))

from base.config_loader import ConfigLoader
from base.data.dataloader import DresdenDataloader
from base.evaluation.evaluate_model import evaluate_cpc
from base.evaluation.result_logger import ResultLogger
from base.logger import Logger
from base.training.scheduled_optim import ScheduledOptim
from base.training.train_model import train_cpc, snapshot


def parse():
    parser = argparse.ArgumentParser(description='training script for the cpc model')
    parser.add_argument('--experiment', '-e', required=True,
                        help='name of experiment to run')

    return parser.parse_args()


def load_cpc_model():
    # create model from cpc_model_name given in config and load it onto configured DEVICE
    model = import_module('.' + config.CPC_MODEL_NAME, 'base.models').Model(config).to(config.DEVICE)
    logger.info('classifier:\n' + str(model))
    # scheduled optimizer containing the pytorch optimizer with parameters from config
    optimizer = ScheduledOptim(
        config.CPC_OPTIMIZER(filter(lambda p: p.requires_grad, model.parameters()),
                             weight_decay=config.CPC_L2_DECAY, **config.CPC_OPTIM_PARAS),
        peak_lr=config.CPC_LR, warmup_epochs=config.CPC_WARMUP_EPOCHS, total_epochs=config.CPC_EPOCHS,
        parameters=config.CPC_S_OPTIM_PARAS, mode=config.CPC_S_OPTIM_MODE)
    return model, optimizer


def training():
    """train cpc model as it is described in config"""
    result_logger = ResultLogger(config)  # wrapper for various methods to log/plot results

    # train dataloader with configured data augmentation and without rebalancing
    dl_train = DresdenDataloader(config, 'train', balanced=False, augment_data=True, classifier=False,
                                 data_fraction=1.0)
    # validation dataloader without modification of loaded data
    dl_valid = DresdenDataloader(config, 'valid', balanced=False, augment_data=False, classifier=False,
                                 data_fraction=1.0)
    # multithreaded pytorch dataloaders with 4 workers each, train data is shuffled
    trainloader = t_data.DataLoader(dl_train, batch_size=config.CPC_BATCH_SIZE, shuffle=True, num_workers=4)
    validationloader = t_data.DataLoader(dl_valid, batch_size=config.CPC_BATCH_SIZE, shuffle=False, num_workers=4)

    model, optimizer = load_cpc_model()

    # save best results for early stopping and snapshot creation of best model
    best_epoch = 0
    best_avg_acc = 0
    # save metrics after each epoch for plots, additional evaluation of epch 0
    metrics = {
        'acc': {'train': np.zeros((config.TIMESTEP, config.CPC_EPOCHS + 1)),
                'valid': np.zeros((config.TIMESTEP, config.CPC_EPOCHS + 1))},
        'loss': {'train': np.zeros((config.CPC_EPOCHS + 1)), 'valid': np.zeros((config.CPC_EPOCHS + 1))}}
    tr_init_loss, tr_init_accs = evaluate_cpc(config, model, trainloader)
    va_init_loss, va_init_accs = evaluate_cpc(config, model, validationloader)
    metrics['loss']['train'][0] = tr_init_loss
    metrics['acc']['train'][:, 0] = tr_init_accs
    metrics['loss']['valid'][0] = va_init_loss
    metrics['acc']['valid'][:, 0] = va_init_accs
    logger.info('[epoch 0] tr_acc: %.4f\t tr_loss: %.4f\t val_acc: %.4f\t val_loss: %.2f\n' % (
        tr_init_accs[-1], tr_init_loss, va_init_accs[-1], va_init_loss))
    logger.info(f'tr accs: {tr_init_accs}, val accs: {va_init_accs}')

    # iterate over EPOCHS, start with epoch 1
    for epoch in range(1, config.CPC_EPOCHS + 1):
        start = time.time()  # measure time each epoch takes

        # train epoch and save metrics
        tr_accs, tr_loss = train_cpc(config, epoch, model, optimizer, trainloader)
        metrics['loss']['train'][epoch] = tr_loss
        metrics['acc']['train'][:, epoch] = tr_accs

        # evaluate epoch and save metrics
        va_loss, va_accs = evaluate_cpc(config, model, validationloader)
        metrics['loss']['valid'][epoch] = va_loss
        metrics['acc']['valid'][:, epoch] = va_accs

        # model from the current epoch better than best model?
        new_best_model = np.mean(va_accs) > best_avg_acc
        # save model if it updates the best model
        if new_best_model:
            best_avg_acc = np.mean(va_accs)
            best_epoch = epoch
            snapshot(config, {
                'model': config.CPC_MODEL_NAME,
                'epoch': epoch,
                'validation_acc': best_avg_acc,
                'state_dict': model.state_dict(),
                'cpc_optimizer': optimizer.state_dict(),
            }, model_type=config.CPC_MODEL_NAME, extra_safe=config.EXTRA_SAFE_MODELS)

        end = time.time()
        logger.info('[epoch {:3d}] execution time: {:.2f}s\t tr_loss: {:.2f}\t tr_accs: {}\n'.format(
            epoch, (end - start), tr_loss, tr_accs))
        logger.info('val_loss: {:.2f}\t val_accs: {}\n'.format(va_loss, va_accs))

        # increase epoch in scheduled optimizer to update the learning rate
        optimizer.inc_epoch()

    # log/plot f1-score course and metrics over all epochs for both datasets
    result_logger.log_metrics(metrics)
    logger.fancy_log('finished training')
    logger.fancy_log('best model on epoch: {} \tacc: {:.4f}'.format(best_epoch, best_avg_acc))


if __name__ == '__main__':
    args = parse()
    config = ConfigLoader(args.experiment, run_name_prefix='cpc')  # load config from experiment

    logger = Logger(config)  # create wrapper for logger
    # create log_file and initialize it with the script arguments and the config
    logger.init_log_file(args, basename(__file__))

    logger.fancy_log('start training of cpc model: {}'.format(config.CPC_MODEL_NAME))
    training()
