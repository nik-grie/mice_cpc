# mice CPC

Repository containing the code to reproduce sleep classification results 
with cpc pretraining.

<br>

## Install Requirements

The experiments where run in an anaconda environment on python 3.7 that can be
recreated using *requirements_win.txt* for windows or *requirements_linux.txt*
for linux.  
Additionaly this repo makes use of a custom package **edfdb**, which can be 
installed from https://github.com/jusjusjus/edfdb  
Since it does not seem like conda can install pytorch from a requirements file
it must also be installed manually. See https://pytorch.org/get-started/locally/
for detailed instructions. 

### Linux

Run:
```bash
conda create --name mice --file requirements_linux.txt
conda activate mice
pip install torch
```

<br>

## Prepare Dataset for ML

Raw electrophysiological signals from each epoch are stored in a pytables
object.  For a mouse recording consisting of several consecutive scored epoch,
we store in columns the mouse id, signal by channel, and the assigned stage
label.  See [our Dresden-data script](scripts/transform_files_dresden.py)
for an example.  The length of epoch segments, sampling rates, and labels have
to be in line with the experiment configuration file such as [this default
one](./config/standard_config.yml).

<br>

## scripts
#### data_viewer.py
script to visualize the data in *dataset* after transformation and to show 
predicted probabilities of the best model of the passed *experiment*

**Parameters:** 
* -e, --experiment: name of experiment to load config from, there must be a file
*\<experiment\>.py* in the config folder
* -d, --dataset: dataset to load data from

#### group_samples_by_stage.py
counts samples per stage and dataset in the transformed `DATA_FILE`

**Parameters:**
* -e, --experiment: name of experiment to load config from, there must be a file
*\<experiment\>.py* in the config folder

#### train_cpc_model.py
trains the cpc model specified in the *experiment* using the configured parameters and 
evaluates it

**Parameters:**
* -e, --experiment: name of experiment to run, there must be a file
*\<experiment\>.py* in the config folder

#### train_classifier.py
trains the classifier and the feature extractor specified in the *experiment* using 
the configured parameters and evaluates it; only the model from the epoch with best 
validation f1-score is saved in `EXPERIMENT_DIR`; additional snapshots can be configured

**Parameters:**
* -e, --experiment: name of experiment to run, there must be a file
*\<experiment\>.py* in the config folder

#### transform_files_dresden.py
script to transform data from the given edf format into a pytables table, also 
performs preprocessing steps like downsampling

**Parameters:**
* -e, --experiment: name of experiment to transform data to, there must be a file
*\<experiment\>.py* in the config folder

### evaluate_experiment.py
evaluates given model(s) in *experiment* on data from passed dataset; results are 
logged in a log file in `EXPERIMENT_DIR` and plots are created in `VISUALS_DIR`;
the scores of all models evaluated are mapped to the configured data fraction
and an overview of this mapping is logged at the end of the evaluation

**Parameters:**
* -e, --experiment: name of experiment to evaluate best model from, there must 
be a file *\<experiment\>.py* in the config folder
* -d, --dataset: dataset to load data from, there must exist a corresponding table
in your transformed data 
* -m, --model: model to evaluate, possible options are 'best' for evaluating only the best model
in `EXPERIMENT_DIR` and 'all' for evalutaing all models saved as snapshot in `EXPERIMENT_DIR/models`

### plots/master/draw_net_cpc.py
draws the architecture of the final model used in my master thesis; code was 
adapted from https://github.com/gwding/draw_convnet; the graphic is saved as a .svg
in the [results folder](./results/plots/master)

### plots/master/draw_signal.py
draws the desired signal of a few epochs without any axes or grids, etc.

### plots/master/plot_accuracies_vs_timesteps.py
plots accuracies of a cpc model against the timesteps; graphic is saved in the 
[results folder](./results/plots/master)

### plots/master/plot_f1_vs_df.py
plots f1 scores of experiments with multiple runs against data fractions;
the scores can be plotted as median of all repetitions or as boxplot;
testscores are saved in [test_f1_vs_df.py](./scripts/plots/master/scores/test_f1_vs_df.py)
and validation scores in [validation_f1_vs_df.py](./scripts/plots/master/scores/validation_f1_vs_df.py);
the plot is saved in the [results folder](./results/plots/master) 

<br>

## experiments

* exp001
    * exp001 (cpc/clas): std cpc experiment with **prediction of random timesteps** per sample;
      classifier with FE mode z_all
    * exp001b (clas): cpc FE from exp001; classifier with FE mode c_last
    * exp001c (clas): cpc FE from exp001; classifier with FE mode c_all
    * exp001d (clas): cpc FE from exp001; classifier with FE mode z_all and fine-tuning of the FE
    * exp001e (clas): cpc FE from exp001; classifier with FE mode c_last and fine-tuning of the FE
    * exp001f (clas): cpc FE from exp001; classifier with FE mode c_all and fine-tuning of the FE
* exp002
    * exp002 (cpc/clas): std cpc experiment with **prediction of last timesteps** per sample;
      classifier with FE mode z_all
    * exp002b (clas): cpc FE from exp002; classifier with FE mode c_last
    * exp002c (clas): cpc FE from exp002; classifier with FE mode c_all
    * exp002d (clas): cpc FE from exp002; classifier with FE mode z_all and fine-tuning of the FE
    * exp002e (clas): cpc FE from exp002; classifier with FE mode c_last and fine-tuning of the FE
    * exp002f (clas): cpc FE from exp002; classifier with FE mode c_all and fine-tuning of the FE
* exp003 (clas): classifier with untrained FE on FE mode c_all

<br>

## configuration parameters
There are many parameters in the code that can be configured in an external yaml
file. The configuration files must be in the [config folder](./config).  
There already files in the directory, the *standard_config.yml*
describing the standard configuration for the data, training, model, etc and folders 
with experiment configs describing various experiments whose results were used in my 
master thesis. As you can see the configuration done in the experiment configs is very 
little compared to the *standard_config*. This is because all additional config files complement
the *standard_config*, i.e. parameters missing in the experiment configs are instead loaded
from *standard_config*.  
The experiments are grouped together depending on their goals in experiment folders, 
e.g. [config/exp001](./config/exp001). These folders contain the actual config files for the
experiments done in the groupings. Results of the experiments in the results folder are
grouped using the same structure, e.g. [results/exp001](./results/exp001), with subfolders
for the actual experiments containing the results.

The following list shows a short description of the possible configuration 
parameters and their mapping to the fields in *ConfigLoader.py*. For examples 
of the configurations see *standard_config.yml*.  
format: \<parameter name in .yml\> (`<mapped name in ConfigLoader>: <dtype>`)

**Info:** only the value `null` in a yaml file is converted to `None` in the code, 
`None` gets converted to a string `'None'`

* general
    * device (`DEVICE: ['cpu' or 'gpu']`): where model is trained and evaluated, 
    can be either 'gpu' or 'cpu'

* dirs
    * cache (`-`): path to a directory used as a cache for various files, like 
    the transformed data file
    * data (`DATA_DIR: str`): path to a directory on your pc containing the original
    data files

* data
  * epoch_duration (`EPOCH_DURATION: [int or float]`): duration of an epoch 
    in seconds
  * sampling_rate (`SAMPLING_RATE: int`): sampling rate of the signal in Hz
  * file (`DATA_FILE: str`): name of the file the transformed data 
    is saved in; file is created in `dirs.cache`
  * split (`DATA_SPLIT: dict`): map containing a key for each dataset with a 
    list of mice; used during data transformation to map edf files to the
    corresponding dataset in the pytables table; there must at least exist
    entries for datasets `train` and `valid`
  * stages (`STAGES: dict`): list of stages you want to train your model on 
    and predict
  * stage_map (`STAGE_MAP: dict`): describes if stages should be mapped to another 
    stage, useful for omitting intermediate stages like 'IS N-R'; format/example:
        * \<stage to map\>: \<stage to map to\>
        * IS N-R: 'Non REM'
  * channels (`CHANNELS: list[str]`): list of channels in your data files, 
    that are used as features
  * balanced_training (`BALANCED_TRAINING: bool`): decides if rebalancing 
    is applied during training (`True`: rebalancing, `False`: no rebalancing)
  * balancing_weights (`BALANCING_WEIGHTS: list[float]`): list of weights 
    used for rebalancing, see *data_table.py* for details
  * epochs_left (`EPOCHS_LEFT: int`): number of additional epochs to the
    left that are loaded together with the epoch to classify; the input of 
    the net consists of `EPOCHS_LEFT`, the epoch to classify and `EPOCHS_RIGHT`
  * epochs_right (`EPOCHS_RIGHT: int`): number of additional epochs to the 
    right, for further information see `EPOCHS_LEFT`
  * normalize (`NORMALIZE_DATA: bool`): flag to decide whether data is to be normalized

* experiment
  * cpc: configuration for the cpc training
    * batch_size (`CPC_BATCH_SIZE: int`): batch size for the train dataloader
    * epochs (`CPC_EPOCHS: int`): number of epochs to train
    * optimizer
      * scheduler: for details and examples see *scheduled_optim.py*
        * warmup_epochs (`CPC_WARMUP_EPOCHS: int`): number of warmup epochs
        * mode (`CPC_S_OPTIM_MODE: ['exp', 'plat', 'step', 'half', null]`): 
          mode used for learning rate decrease after warmup
        * parameters (`CPC_S_OPTIM_PARAS: dict`): list of parameters for the 
          selected mode
      * learning_rate (`CPC_LR: float`): peak learning rate after 
        warmup
      * class (`CPC_OPTIMIZER: Optimizer`): the optimizer; subclass of 
        `torch.optim.optimizer.Optimizer`
      * parameters (`CPC_OPTIM_PARAS: dict`): parameters for the `OPTIMIZER` 
        like `eps`, `betas`, etc. 
      * l2_weight_decay (`CPC_L2_DECAY: float`): factor of applied L2 decay
  * clas: configuration for the classifier training
    * data_fractions (`DATA_FRACTIONS: list[float]`): list of fractions of train data to use for 
      training, validation data remains the same
    * data_fraction_strat (`DATA_FRACTION_STRAT: ['uniform', null]`): strategy 
      for data fractions, currently supports only null (no strategy, take the 
      same data fraction of each stage) and 'uniform' (take the same number of 
      samples from each stage so the total number of samples corresponds to the
      data fraction)
    * batch_size (`CLAS_BATCH_SIZE: int`): batch size for the train dataloader
    * epochs (`CLAS_EPOCHS: int`): number of epochs to train
    * optimizer
      * scheduler: for details and examples see *scheduled_optim.py*
        * warmup_epochs (`CLAS_WARMUP_EPOCHS: int`): number of warmup epochs
        * mode (`CLAS_S_OPTIM_MODE: ['exp', 'plat', 'step', 'half', null]`): 
          mode used for learning rate decrease after warmup
        * parameters (`CLAS_S_OPTIM_PARAS: dict`): list of parameters for the 
          selected mode
      * learning_rate (`CLAS_LR: float`): peak learning rate after 
        warmup
      * cpc_learning_rate (`CLAS_CPC_LR: float`): peak learning rate after warmup used to train the cpc model
        when fine-tuning
      * class (`CLAS_OPTIMIZER: Optimizer`): the optimizer; subclass of 
        `torch.optim.optimizer.Optimizer`
      * parameters (`CLAS_OPTIM_PARAS: dict`): parameters for the `OPTIMIZER` 
        like `eps`, `betas`, etc. 
      * l2_weight_decay (`CLAS_L2_DECAY: float`): factor of applied L2 decay
  * log_interval (`LOG_INTERVAL: int`): percentage of data in trainloader 
    after which a log message is created in a training epoch
  * additional_model_safe (`EXTRA_SAFE_MODELS: bool`): if set to `True` 
    additional snapshots of the best model of a run are saved in `MODELS_DIR`;
    should be activated, when training on multiple data fractions, because otherwise the models
    trained on the data fractions are overwritten and lost; additional models can be evaluated 
    using the [evaluate_experiment.py](scripts/evaluate_experiment.py) script with option *-m all*

  * evaluation
    * batch_size (`BATCH_SIZE_EVAL: int`): batch size used during evaluation

  * model
    * cpc: configuration of the cpc model
      * filters (`FILTERS: int`): number of filters used in conv layers of the 
        feature extractor
      * timestep (`TIMESTEP: int`): number of timesteps the cpc model predicts into the future 
      * gru_hidden (`GRU_HIDDEN: int`): size of the hidden layer of the gru
      * first_norm (`CPC_FIRST_NORM: bool`): whether an additional batchnorm layer is used as first layer
      * num_predictions (`NUM_PREDICTION: int`): number of predictions of the timesteps per sample
      * name (`CPC_MODEL_NAME: str`): name of the model to be trained, must be the 
        name of a python file in `base.models`; the name of the class in the file
        must be 'Model'
    * clas: configuration of the classifier
      * dropout (`CLAS_DROPOUT: list[float]`): list of dropout 
        probabilities applied in the classifier
      * name (`CLAS_MODEL_NAME: str`): name of the model to be trained, must be the 
        name of a python file in [base.models](base/models); the name of the class in the file
        must be 'Model'
      * cpc_model (`CLAS_CPC_MODEL_FILE: ['same', 'none', str]`): exp and model name of the cpc to be 
        used as feature extractor,
        possible values are ['same' (load cpc model from current exp), 
        'none' (untrained feature extractor), '<exp>/<model-name>' 
        (cpc model from exp, e.g.: 'exp001/cpc_4conv_1gru-best.pth')]
      * feature_mode (`CLAS_FEATURE_MODE: ['z_all', 'c_last', 'c_all'`): 
        mode of feature extraction, possible values are 
        ['z_all' (use all encodings), 'c_last' (use last context only), 'c_all' (use all contexts)]
      * first_norm (`CLAS_FIRST_NORM: bool`): whether an additional batchnorm layer is used as first layer

  * data_augmentation:
    * cpc: data augmentation during cpc training
      * gain (`CPC_GAIN: float`): parameter for signal amplitude amplification
      * flip (`CPC_FLIP: float`): probability for vertically flipping single 
        datapoints in the signal
      * flip_all (`CPC_FLIP_ALL: float`): probability for vertically flipping the 
        whole signal
      * flip_hori (`CPC_FLIP_HORI: float`): probability for horizontally flipping the
        whole signal
      * window_warp_size (`CPC_WINDOW_WARP_SIZE: float`): factor for selection of the 
        new window size in window warping
      * time_shift (`CPC_TIME_SHIFT: float`): factor to determine the random amount 
        of time a signal is shifted
    * clas: data augmentation during training of classifier
      * gain (`CLAS_GAIN: float`): parameter for signal amplitude amplification
      * flip (`CLAS_FLIP: float`): probability for vertically flipping single 
        datapoints in the signal
      * flip_all (`CLAS_FLIP_ALL: float`): probability for vertically flipping the 
        whole signal
      * flip_hori (`CLAS_FLIP_HORI: float`): probability for horizontally flipping the
        whole signal
      * window_warp_size (`CLAS_WINDOW_WARP_SIZE: float`): factor for selection of the 
        new window size in window warping
      * time_shift (`CLAS_TIME_SHIFT: float`): factor to determine the random amount 
        of time a signal is shifted
