import collections
import time
from importlib import import_module
from os import makedirs
from os.path import dirname, join, realpath

import numpy as np
import yaml
from torch import optim


def update_dict(d_to_update: dict, update: dict):
    """method to update a dict with the entries of another dict

    `d_to_update` is updated by `update`"""
    for k, v in update.items():
        if isinstance(v, collections.abc.Mapping):
            d_to_update[k] = update_dict(d_to_update.get(k, {}), v)
        else:
            d_to_update[k] = v
    return d_to_update


class ConfigLoader:
    def __init__(self, experiment='standard_config', run_name_prefix='', create_dirs=True):
        """general class to load config from yaml files into fields of an instance of this class

        first the config from standard_config.yml is loaded and then updated with the entries in the config file
        specified by `experiment`

        for a description of the various configurations see README.md

        Args:
            experiment (str): name of the config file to load without the .yml file extension; file must be in folder
                'config'
            run_name_prefix (str): prefix for the run_name, used to differentiate between cpc and classifier results
            create_dirs (bool): if set to False EXPERIMENT_DIR, MODELS_DIR and VISUALS_DIR are not created
        """
        base_dir = realpath(join(dirname(__file__), '..'))

        self.experiment = experiment
        base_exp = experiment[:6]
        config = self.load_config()

        self.RUN_NAME = run_name_prefix + ('_' if run_name_prefix != '' else '') + experiment + '_' + time.strftime(
            "%Y-%m-%d_%H-%M-%S")
        """identifier for the run of the experiment, used in log_file and `VISUALS_DIR`"""

        # general
        self.DEVICE = config['general']['device']
        assert self.DEVICE in ['cpu', 'cuda'], 'DEVICE only support `cpu` or `cuda`'

        # dirs
        self.EXPERIMENT_DIR = join(base_dir, 'results', base_exp, self.experiment)
        """general directory with results from experiment"""
        self.MODELS_DIR = join(self.EXPERIMENT_DIR, 'models')
        """directory in `EXPERIMENT_DIR` for `extra_safe` models"""
        self.VISUALS_DIR = join(self.EXPERIMENT_DIR, 'visuals', self.RUN_NAME)
        """directory in `EXPERIMENT_DIR` for all generated plots"""
        if create_dirs:
            makedirs(self.EXPERIMENT_DIR, exist_ok=True)
            makedirs(self.MODELS_DIR, exist_ok=True)
            makedirs(self.VISUALS_DIR, exist_ok=True)

        self.DATA_DIR = realpath(config['dirs']['data'])
        cache_dir = config['dirs']['cache']
        makedirs(cache_dir, exist_ok=True)

        # data
        data_config = config['data']
        self.EPOCH_DURATION = data_config['epoch_duration']
        assert type(self.EPOCH_DURATION) in [int, float]
        self.SAMPLING_RATE = data_config['sampling_rate']
        assert type(self.EPOCH_DURATION) is int
        self.DATA_FILE = join(cache_dir, data_config['file'])
        self.DATA_SPLIT = data_config['split']
        assert 'train' in self.DATA_SPLIT and 'valid' in self.DATA_SPLIT, \
            'DATA_SPLIT must at least contain keys train and valid'
        self.STAGES = data_config['stages']
        self.STAGE_MAP = data_config['stage_map']
        for k in self.STAGE_MAP:
            msg = 'You are trying to map STAGE {} to STAGE {} that does not exist in experiment.data.stages'
            assert self.STAGE_MAP[k] in self.STAGES + [None], msg.format(k, self.STAGE_MAP[k])
        for s in self.STAGES:
            assert s in self.STAGE_MAP.values(), 'STAGE {} has no mapping in STAGE_MAP'.format(s)
        self.CHANNELS = data_config['channels']
        self.BALANCED_TRAINING = data_config['balanced_training']
        assert type(self.BALANCED_TRAINING) is bool
        self.BALANCING_WEIGHTS = data_config['balancing_weights']
        assert np.all([w >= 0 for w in self.BALANCING_WEIGHTS]), 'BALANCING_WEIGHTS must be non negative'
        self.EPOCHS_LEFT = data_config['epochs_left']
        assert type(self.EPOCHS_LEFT) is int
        self.EPOCHS_RIGHT = data_config['epochs_right']
        assert type(self.EPOCHS_RIGHT) is int
        self.NORMALIZE_DATA = data_config['normalize']
        assert type(self.NORMALIZE_DATA) is bool

        # experiment
        exp_config = config['experiment']
        # cpc
        cpc_config = exp_config['cpc']
        self.CPC_BATCH_SIZE = cpc_config['batch_size']
        assert type(self.CPC_BATCH_SIZE) is int
        self.CPC_EPOCHS = cpc_config['epochs']
        assert type(self.CPC_EPOCHS) is int
        self.CPC_WARMUP_EPOCHS = cpc_config['optimizer']['scheduler']['warmup_epochs']
        assert type(self.CPC_WARMUP_EPOCHS) is int
        self.CPC_S_OPTIM_MODE = cpc_config['optimizer']['scheduler']['mode']
        assert self.CPC_S_OPTIM_MODE in ['step', 'exp', 'half', 'plat', None], \
            'CPC_S_OPTIM_MODE is not one of ["step", "exp", "half", "plat", None]'
        self.CPC_S_OPTIM_PARAS = cpc_config['optimizer']['scheduler']['parameters']
        assert type(self.CPC_S_OPTIM_PARAS) in [list, type(None)], 'CPC_S_OPTIM_PARAS must be a list or None'
        self.CPC_LR = cpc_config['optimizer']['learning_rate']
        assert type(self.CPC_LR) is float
        self.CPC_OPTIMIZER = getattr(optim, cpc_config['optimizer']['class'])
        assert issubclass(self.CPC_OPTIMIZER, optim.Optimizer), \
            'CPC_OPTIMIZER must be a subtype of optim.optimizer.Optimizer'
        self.CPC_OPTIM_PARAS = cpc_config['optimizer']['parameters']
        assert type(self.CPC_OPTIM_PARAS) is dict, 'CPC_OPTIM_PARAS must be a dict'
        self.CPC_L2_DECAY = cpc_config['optimizer']['l2_weight_decay']
        assert type(self.CPC_L2_DECAY) in [float, int]

        # classifier
        clas_config = exp_config['clas']
        self.DATA_FRACTIONS = clas_config['data_fractions']
        assert type(self.DATA_FRACTIONS) is list
        for df in self.DATA_FRACTIONS:
            assert 0 < df <= 1, 'DATA_FRACTIONS must be in (0,1]'
        self.DATA_FRACTION_STRAT = clas_config['data_fraction_strat']
        assert self.DATA_FRACTION_STRAT in ['uniform', None], \
            'currently only "uniform" or None is supported as a data fraction strategy'
        self.CLAS_BATCH_SIZE = clas_config['batch_size']
        assert type(self.CLAS_BATCH_SIZE) is int
        self.CLAS_EPOCHS = clas_config['epochs']
        assert type(self.CLAS_EPOCHS) is int
        self.CLAS_WARMUP_EPOCHS = clas_config['optimizer']['scheduler']['warmup_epochs']
        assert type(self.CLAS_WARMUP_EPOCHS) is int
        self.CLAS_S_OPTIM_MODE = clas_config['optimizer']['scheduler']['mode']
        assert self.CLAS_S_OPTIM_MODE in ['step', 'exp', 'half', 'plat', None], \
            'CLAS_S_OPTIM_MODE is not one of ["step", "exp", "half", "plat", None]'
        self.CLAS_S_OPTIM_PARAS = clas_config['optimizer']['scheduler']['parameters']
        assert type(self.CLAS_S_OPTIM_PARAS) in [list, type(None)], 'CLAS_S_OPTIM_PARAS must be a list or None'
        self.CLAS_LR = clas_config['optimizer']['learning_rate']
        assert type(self.CLAS_LR) is float
        self.CLAS_CPC_LR = clas_config['optimizer']['cpc_learning_rate']
        assert type(self.CLAS_CPC_LR) in [float, type(None)]
        self.CLAS_OPTIMIZER = getattr(optim, clas_config['optimizer']['class'])
        assert issubclass(self.CLAS_OPTIMIZER, optim.Optimizer), \
            'CLAS_OPTIMIZER must be a subtype of optim.optimizer.Optimizer'
        self.CLAS_OPTIM_PARAS = clas_config['optimizer']['parameters']
        assert type(self.CLAS_OPTIM_PARAS) is dict, 'CLAS_OPTIM_PARAS must be a dict'
        self.CLAS_L2_DECAY = clas_config['optimizer']['l2_weight_decay']
        assert type(self.CLAS_L2_DECAY) in [float, int]

        # general experiment
        self.LOG_INTERVAL = exp_config['log_interval']
        assert type(self.LOG_INTERVAL) is int
        self.EXTRA_SAFE_MODELS = exp_config['additional_model_safe']
        assert type(self.EXTRA_SAFE_MODELS) is bool

        # evaluation
        self.BATCH_SIZE_EVAL = config['experiment']['evaluation']['batch_size']
        assert type(self.BATCH_SIZE_EVAL) is int

        # model cpc
        model_cpc_config = exp_config['model']['cpc']
        self.FILTERS = model_cpc_config['filters']
        assert type(self.FILTERS) is int
        self.TIMESTEP = model_cpc_config['timestep']
        assert type(self.TIMESTEP) is int
        self.GRU_HIDDEN = model_cpc_config['gru_hidden']
        assert type(self.GRU_HIDDEN) is int
        self.CPC_FIRST_NORM = model_cpc_config['first_norm']
        assert type(self.CPC_FIRST_NORM) is bool
        self.NUM_PREDICTION = model_cpc_config['num_predictions']
        assert type(self.NUM_PREDICTION) is int
        self.CPC_MODEL_NAME = model_cpc_config['name']
        try:
            module = import_module('.' + self.CPC_MODEL_NAME, 'base.models')
            assert getattr(module, 'Model') is not None, 'name of cpc model class must be "Model"'
        except:
            assert False, 'CPC_MODEL_NAME does not exist'

        # model classifier
        model_clas_config = exp_config['model']['clas']
        self.CLAS_DROPOUT = model_clas_config['dropout']
        assert type(self.CLAS_DROPOUT) is list and np.all([type(o) is float for o in self.CLAS_DROPOUT])
        self.CLAS_MODEL_NAME = model_clas_config['name']
        try:
            module = import_module('.' + self.CLAS_MODEL_NAME, 'base.models')
            assert getattr(module, 'Model') is not None, 'name of classifier model class must be "Model"'
        except:
            assert False
        self.CLAS_CPC_MODEL_FILE = model_clas_config['cpc_model']
        assert type(self.CLAS_CPC_MODEL_FILE) is str
        self.CLAS_FEATURE_MODE = model_clas_config['feature_mode']
        assert self.CLAS_FEATURE_MODE in ['c_last', 'z_all', 'c_all'], \
            'CLAS_FEATURE_MODE must be one of ["c_last", "z_all", "c_all"]'
        self.CLAS_FIRST_NORM = model_clas_config['first_norm']
        assert type(self.CLAS_FIRST_NORM) is bool

        # data augmentation cpc
        data_aug_cpc_config = exp_config['data_augmentation']['cpc']
        self.CPC_GAIN = data_aug_cpc_config['gain']
        assert 0 <= self.CPC_GAIN <= 1, 'CPC_GAIN must be in [0,1]'
        self.CPC_FLIP = data_aug_cpc_config['flip']
        assert 0 <= self.CPC_FLIP <= 1, 'CPC_FLIP must be in [0,1]'
        self.CPC_FLIP_ALL = data_aug_cpc_config['flip_all']
        assert 0 <= self.CPC_FLIP_ALL <= 1, 'CPC_FLIP_ALL must be in [0,1]'
        self.CPC_WINDOW_WARP_SIZE = data_aug_cpc_config['window_warp_size']
        assert 0 <= self.CPC_WINDOW_WARP_SIZE <= 1, 'CPC_WINDOW_WARP_SIZE must be in [0,1]'
        self.CPC_FLIP_HORI = data_aug_cpc_config['flip_hori']
        assert 0 <= self.CPC_FLIP_HORI <= 1, 'CPC_FLIP_HORI must be in [0,1]'
        self.CPC_TIME_SHIFT = data_aug_cpc_config['time_shift']
        assert 0 <= self.CPC_TIME_SHIFT <= 1, 'CPC_TIME_SHIFT must be in [0,1]'

        # data augmentation classifier
        data_aug_clas_config = exp_config['data_augmentation']['clas']
        self.CLAS_GAIN = data_aug_clas_config['gain']
        assert 0 <= self.CLAS_GAIN <= 1, 'CLAS_GAIN must be in [0,1]'
        self.CLAS_FLIP = data_aug_clas_config['flip']
        assert 0 <= self.CLAS_FLIP <= 1, 'CLAS_FLIP must be in [0,1]'
        self.CLAS_FLIP_ALL = data_aug_clas_config['flip_all']
        assert 0 <= self.CLAS_FLIP_ALL <= 1, 'CLAS_FLIP_ALL must be in [0,1]'
        self.CLAS_WINDOW_WARP_SIZE = data_aug_clas_config['window_warp_size']
        assert 0 <= self.CLAS_WINDOW_WARP_SIZE <= 1, 'CLAS_WINDOW_WARP_SIZE must be in [0,1]'
        self.CLAS_FLIP_HORI = data_aug_clas_config['flip_hori']
        assert 0 <= self.CLAS_FLIP_HORI <= 1, 'CLAS_FLIP_HORI must be in [0,1]'
        self.CLAS_TIME_SHIFT = data_aug_clas_config['time_shift']
        assert 0 <= self.CLAS_TIME_SHIFT <= 1, 'CLAS_TIME_SHIFT must be in [0,1]'

    def load_config(self):
        """loads config from standard_config.yml and updates it with <experiment>.yml"""
        base_dir = realpath(join(dirname(__file__), '..'))
        with open(join(base_dir, 'config', 'standard_config.yml'), 'r') as ymlfile:
            config = yaml.safe_load(ymlfile)
        if self.experiment[:3] == 'exp':
            with open(join(base_dir, 'config', self.experiment[:6], self.experiment + '.yml'), 'r') as ymlfile:
                config = update_dict(config, yaml.safe_load(ymlfile))
        return config
