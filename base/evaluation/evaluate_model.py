import numpy as np
import torch
from torch import nn

from base.config_loader import ConfigLoader


def evaluate_cpc(config, model, validationloader):
    """ evaluate cpc model using the data given by validationloader

    Args:
        config (ConfigLoader): config of the experiment
        model (nn.Module): cpc model to be evaluated
        validationloader (torch.utils.data.DataLoader): dataloader containing the data to be used for evaluation

    Returns:
        (float, np.ndarray): loss and accuracies for all timesteps averaged over all evaluation samples
    """
    model = model.eval()
    total_loss = 0
    # calculate accuracy for every timestep
    total_accs = np.zeros(config.TIMESTEP)

    with torch.no_grad():
        for data in validationloader:
            features, _ = data  # labels are not needed, unsupervised training
            batch = features.size()[0]
            features = features.to(config.DEVICE)

            hidden = model.init_hidden(batch)
            accs, loss, hidden = model(features, hidden)

            total_loss += loss * batch
            total_accs += accs * batch

        total_loss /= len(validationloader.dataset)
        total_accs /= len(validationloader.dataset)

    return total_loss, total_accs


def evaluate_classifier(config, cpc_model, clas_model, validationloader):
    """ evaluate model using the data given by validationloader

    Args:
        config (ConfigLoader): config of the experiment
        cpc_model (nn.Module): cpc model used as a feature extractor for the classifier
        clas_model (nn.Module): classifier to be evaluated
        validationloader (torch.utils.data.DataLoader): dataloader containing the data to be used for evaluation

    Returns:
        (dict, float): dict with actual and predicted labels, actual labels are accessible with the key 'actual' and the
            predicted labels with 'predicted'; float containing loss averaged over all evaluation samples
    """
    cpc_model.eval()
    clas_model = clas_model.eval()
    predicted_labels = np.empty(0, dtype='int')
    actual_labels = np.empty(0, dtype='int')
    loss = torch.zeros(1).to(config.DEVICE)

    with torch.no_grad():
        for data in validationloader:
            features, labels = data
            features = features.to(config.DEVICE)
            labels = labels.long().to(config.DEVICE)

            # cpc feature extractor
            hidden = cpc_model.init_hidden(len(features))
            features = cpc_model.predict(features, hidden)

            # classifier
            outputs = clas_model(features)

            criterion = nn.NLLLoss()
            loss += criterion(outputs, labels)

            _, predicted_labels_i = torch.max(outputs, dim=1)

            predicted_labels = np.r_[predicted_labels, predicted_labels_i.tolist()]
            actual_labels = np.r_[actual_labels, labels.tolist()]

    return {'actual': actual_labels, 'predicted': predicted_labels}, loss.item() / len(validationloader)
