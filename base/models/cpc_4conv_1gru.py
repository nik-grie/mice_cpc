import numpy as np
import torch
import torch.nn as nn

from base.config_loader import ConfigLoader
from base.utilities import calculate_tensor_size_after_convs


class Model(nn.Module):
    """simple cpc model with an encoder consisting of 4 conv layers, a GRU with one layer as autoregressive model and
    simple linear models as predictors\n
    the model makes one prediction per sample using TIMESTEP prediction steps starting at a random encoding

    a BN layer as first layer can be configured using CPC_FIRST_NORM\n
    """

    def __init__(self, config: ConfigLoader):
        super(Model, self).__init__()

        self.config = config
        # length of a sample in datapoints, i.e. 4s/epoch * 5epochs * 64Hz = 1280
        self.seq_len = self.config.EPOCH_DURATION * (
                self.config.EPOCHS_LEFT + self.config.EPOCHS_RIGHT + 1) * self.config.SAMPLING_RATE
        self.gru_hidden = self.config.GRU_HIDDEN

        # build encoder
        layers = [  # downsampling factor 16: 1280 --> 80
            nn.Conv1d(len(config.CHANNELS), config.FILTERS, 5, 2, padding=2, bias=False),
            nn.BatchNorm1d(config.FILTERS),
            nn.LeakyReLU(inplace=True),
            nn.Conv1d(config.FILTERS, config.FILTERS, 5, 2, padding=2, bias=False),
            nn.BatchNorm1d(config.FILTERS),
            nn.LeakyReLU(inplace=True),
            nn.Conv1d(config.FILTERS, config.FILTERS, 4, 2, padding=1, bias=False),
            nn.BatchNorm1d(config.FILTERS),
            nn.LeakyReLU(inplace=True),
            nn.Conv1d(config.FILTERS, config.FILTERS, 4, 2, padding=1, bias=False),
            nn.BatchNorm1d(config.FILTERS),
            nn.LeakyReLU(inplace=True)
        ]
        # add batchnorm in front if configured
        if config.CPC_FIRST_NORM:
            layers = [nn.BatchNorm1d(len(config.CHANNELS))] + layers
        self.encoder = nn.Sequential(*layers)

        self.gru = nn.GRU(config.FILTERS, self.gru_hidden, num_layers=1, batch_first=True)
        self.Wk = nn.ModuleList([nn.Linear(self.gru_hidden, config.FILTERS) for i in range(config.TIMESTEP)])
        self.softmax = nn.Softmax(dim=1)

        # initialize weights using He initialization
        def _weights_init(m):
            if isinstance(m, nn.Linear):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            if isinstance(m, nn.Conv1d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        # initialize gru
        for layer_p in self.gru._all_weights:
            for p in layer_p:
                if 'weight' in p:
                    nn.init.kaiming_normal_(self.gru.__getattr__(p), mode='fan_out', nonlinearity='relu')

        self.apply(_weights_init)

    def init_hidden(self, batch_size):
        """initialize hidden state of single layer GRU"""
        return torch.zeros(1, batch_size, self.gru_hidden).to(self.config.DEVICE)

    def forward(self, x, hidden):
        """
        Returns:
            (np.ndarray, float, torch.Tensor): array of accuracies for every timestep; nce loss function averaged over
            timesteps; Tensor with hidden state of the GRU
        """
        batch = x.size()[0]
        # input sequence is N*C*L, e.g. batch*len(CHANNELS)*1280
        z = self.encoder(x)
        # encoded sequence is N*C*L, e.g. batch*FILTERS*80
        # reshape to N*L*C for GRU, e.g. batch*80*FILTERS
        z = z.transpose(1, 2)
        # encoding to base predictions on (context is built up to that encoding and this context is used to make
        # predictions for the next TIMESTEP encodings)
        prediction_base = torch.randint(z.shape[1] - self.config.TIMESTEP,
                                        size=(1,)).long()  # randomly pick time stamps

        # use encodings up to the prediction base to calculate the context
        forward_seq = z[:, :prediction_base + 1, :]  # e.g. size batch*'50'*FILTERS
        output, hidden = self.gru(forward_seq, hidden)  # output size e.g. batch*'50'*GRU_HIDDEN
        context = output[:, -1, :]  # context size batch*GRU_HIDDEN

        # predict the next TIMESTEP encodings after the context using simple linear transformations
        pred = torch.empty(
            (self.config.TIMESTEP, batch, self.config.FILTERS)).float().to(
            self.config.DEVICE)  # size TIMESTEP*batch*FILTERS
        for i in np.arange(0, self.config.TIMESTEP):
            linear = self.Wk[i]
            pred[i] = linear(context)  # Wk*context size batch*FILTERS

        # calculate NCE and accuracies
        accuracies = np.empty(self.config.TIMESTEP)
        nce = 0
        for i in torch.arange(0, self.config.TIMESTEP):
            pos_idxs = torch.arange(0, batch).to(self.config.DEVICE)
            total = torch.mm(pred[i],
                             torch.transpose(z[:, (prediction_base + i + 1).item(), :], 0, 1))  # size batch*batch
            # number of predictions where total "found" the right sample
            correct = torch.sum(torch.eq(torch.argmax(self.softmax(total), dim=1), pos_idxs))
            accuracies[i] = 1. * correct.item() / batch

            nce += nn.CrossEntropyLoss()(total, pos_idxs)
        # average NCE over all predictions
        nce /= self.config.TIMESTEP

        # return accuracies
        return accuracies, nce, hidden

    def predict(self, x, hidden):
        """build feature vector for input `x` based on the configured FEATURE_MODE

        z_all: features are the encodings returned by the encoder
        c_last: features are the context returned by the GRU applied to all encodings"""
        batch = x.size()[0]
        # input sequence is N*C*L, e.g. batch*3*1280
        z = self.encoder(x)
        # encoded sequence is N*C*L, e.g. batch*self.config.FILTERS*80
        # reshape to N*L*C for GRU, e.g. batch*80*self.config.FILTERS
        z = z.transpose(1, 2)

        if self.config.CLAS_FEATURE_MODE == 'z_all':
            return z.reshape(batch, -1)  # return every encoding in a flattened vector
        else:
            output, hidden = self.gru(z, hidden)  # output size e.g. batch*75*self.gru_hidden
            if self.config.CLAS_FEATURE_MODE == 'c_last':
                return output[:, -1, :]  # return context of the last frame
            elif self.config.CLAS_FEATURE_MODE == 'c_all':
                return output.reshape(batch, -1)  # return context of every frame in a flattened vector

    def get_feature_size(self):
        """
        Returns:
            int: number of encodings the last conv layer returns
        """
        return calculate_tensor_size_after_convs(self.seq_len, [5, 5, 4, 4], [2, 2, 2, 2], [2, 2, 1, 1])
