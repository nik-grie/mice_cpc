import torch.nn as nn
import torch.nn.functional as F

from base.config_loader import ConfigLoader


class Model(nn.Module):
    """simple linear classifier consisting of a single fully connected layer with configurable dropout and batchnorm
    layers"""

    def __init__(self, config: ConfigLoader, feature_size):
        super(Model, self).__init__()

        # determine number of features delivered by the feature extractor based on wether it returns the last context,
        # all contexts or all encodings
        input_size = config.GRU_HIDDEN
        if config.CLAS_FEATURE_MODE == 'c_last':  # only last context
            input_size = config.GRU_HIDDEN  # gru hidden size
        elif config.CLAS_FEATURE_MODE == 'c_all':  # all contexts
            input_size = config.GRU_HIDDEN * feature_size  # gru hidden size * downsampled input size
        elif config.CLAS_FEATURE_MODE == 'z_all':  # all encodings
            input_size = feature_size * config.FILTERS  # downsampled input size * number of filters

        # build classifier, add batchnorm layer in front if configured
        layers = [
            nn.Dropout(p=config.CLAS_DROPOUT[0]),
            nn.Linear(input_size, len(config.STAGES))
        ]
        if config.CLAS_FIRST_NORM:
            layers = [nn.BatchNorm1d(input_size)] + layers
        self.classifier = nn.Sequential(*layers)

        # initialize weights using He initialization
        def _weights_init(m):
            if isinstance(m, nn.Linear):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        self.apply(_weights_init)

    def forward(self, x):
        x = self.classifier(x)
        return F.log_softmax(x, dim=1)
