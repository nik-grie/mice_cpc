import logging
from os.path import join

import numpy as np
import torch
import torch.nn as nn

from base.config_loader import ConfigLoader
from base.training.scheduled_optim import ScheduledOptim

logger = logging.getLogger('cpc')


def train_cpc(config, epoch, model, optimizer, trainloader):
    """train cpc `model` in `epoch` using the train data from `trainloader`

    Args:
        config (ConfigLoader): configuration of the experiment
        epoch (int): epoch that is trained (just for logging purposes)
        model (nn.Module): cpc model that is trained
        optimizer (ScheduledOptim): scheduled optimizer containing the optimizer used for training the cpc model
        trainloader (torch.utils.data.DataLoader): dataloader with the data to train on

    Returns:
        (np.ndarray, float): accuracies for all timesteps and loss averaged over all training samples
    """
    model.train()
    # frequency of logs (every LOG_INTERVAL% of data in trainloader)
    log_fr = max(int(config.LOG_INTERVAL / 100. * len(trainloader)), 1)

    # global metrics over all train data
    total_loss = 0
    total_accs = np.zeros(config.TIMESTEP)
    for i, data in enumerate(trainloader, 0):
        # get the inputs; data is a list of [inputs, labels], labels are not needed for unsupervised training
        features, _ = data
        features = features.to(config.DEVICE)

        optimizer.zero_grad()

        # get metrics for features and update the global metrics
        hidden = model.init_hidden(len(features))
        accs, loss, hidden = model(features, hidden)

        total_loss += loss * features.size(0)
        total_accs += accs * features.size(0)

        # perform gradient update and update learning rate
        loss.backward()
        optimizer.step()
        lr = optimizer.update_learning_rate()

        if i % log_fr == log_fr - 1:
            logger.info('train epoch: {} [{}/{} ({:.0f}%)]\tlr: {:.2e}\taccuracy: {:.4f}\tloss: {:.6f}'.format(
                epoch, i * len(features), len(trainloader.dataset), 100. * i / len(trainloader), lr, accs[-1],
                loss.item()))
    return total_accs / len(trainloader.dataset), total_loss / len(trainloader.dataset)


def train_classifier(config, epoch, cpc_model, clas_model, cpc_optimizer, clas_optimizer, trainloader):
    """train classifier in `epoch` using the data from `trainloader`

    Args:
        config (ConfigLoader): configuration of the experiment
        epoch (int): epoch that is trained (just for logging purposes)
        cpc_model (nn.Module): cpc model to use as a feature extractor
        clas_model (nn.Module): classifier to train
        cpc_optimizer (ScheduledOptim): scheduled optimizer containing the optimizer used for fine-tuning the cpc model
        clas_optimizer (ScheduledOptim): scheduled optimizer containing the optimizer used for training the classifier
        trainloader (torch.utils.data.DataLoader): dataloader with the data to train on

    Returns:
        (dict, float): dict with 'actual' and 'predicted' labels and loss of last minibatch
    """
    # if CLAS_CPC_LR is not None the cpc model is fine-tuned
    if config.CLAS_CPC_LR is None:
        cpc_model.eval()
    else:
        cpc_model.train()
    clas_model.train()  # just as a precaution, so BN and dropout are active
    predicted_labels = np.empty(0, dtype='int')  # save predicted and actual labels to return
    actual_labels = np.empty(0, dtype='int')
    # frequency of logs (every LOG_INTERVAL% of data in trainloader)
    log_fr = max(int(config.LOG_INTERVAL / 100. * len(trainloader)), 1)
    loss = torch.zeros(1)

    for i, data in enumerate(trainloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        features, labels = data
        features = features.to(config.DEVICE)
        labels = labels.long().to(config.DEVICE)
        # skip batches with only one sample, because otherwise the BN layers do not work
        if features.shape[0] == 1: continue

        # zero the parameter gradients, only do so for the cpc optimizer if the cpc model is to be fine-tuned
        if config.CLAS_CPC_LR is not None:
            cpc_optimizer.zero_grad()
        clas_optimizer.zero_grad()

        # extract features from cpc model
        hidden = cpc_model.init_hidden(len(features))
        features = cpc_model.predict(features, hidden)

        # get predictions from classifier
        outputs = clas_model(features)

        criterion = nn.NLLLoss()
        loss = criterion(outputs, labels)

        loss.backward()
        # clip the gradients to avoid exploding gradients
        nn.utils.clip_grad_norm_(clas_model.parameters(), 0.1, 'inf')

        # perform gradient update for cpc model if it is to be fine-tuned
        if config.CLAS_CPC_LR is not None:
            nn.utils.clip_grad_norm_(cpc_model.parameters(), 0.1, 'inf')
            cpc_optimizer.step()
            cpc_optimizer.update_learning_rate()

        clas_optimizer.step()
        lr = clas_optimizer.update_learning_rate()  # update lr after each step (minibatch)

        # determine predicted labels and save them together with actual labels
        _, predicted_labels_i = torch.max(outputs, dim=1)
        predicted_labels = np.r_[predicted_labels, predicted_labels_i.tolist()]
        actual_labels = np.r_[actual_labels, labels.tolist()]

        # log various information every log_fr minibatches
        if i % log_fr == log_fr - 1:
            logger.info('train epoch: {} [{}/{} ({:.0f}%)]\tlr: {:.2e}\tloss: {:.6f}'.format(
                epoch, i * len(features), len(trainloader.dataset),
                       100. * i / len(trainloader), lr, loss.item()))

    return {'actual': actual_labels, 'predicted': predicted_labels}, loss.item()


def snapshot(config: ConfigLoader, model_state, model_type, extra_safe=False):
    """save a snapshot of the model_state in EXPERIMENT_DIR

    if extra_safe is set to True, an additional snapshot is saved in MODELS_DIR (useful because the name for the
    standard snapshot is somewhat generic and the file would be overwritten with a new run of the exp"""
    snapshot_file = join(config.EXPERIMENT_DIR, model_type + '-best.pth')
    torch.save(model_state, snapshot_file)
    logger.info('snapshot saved to {}'.format(snapshot_file))

    if extra_safe:
        add_snapshot_file = join(config.MODELS_DIR, model_type + '-' + config.RUN_NAME + '.pth')
        torch.save(model_state, add_snapshot_file)
        logger.info('additional snapshot saved to {}'.format(add_snapshot_file))
